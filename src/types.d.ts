// Extra types not covered by foundry-vtt-types
declare let KeybindLib: any;

// Instead of importing complex helpers
declare type AnyDocumentType =
  | typeof Actor
  | typeof Item
  | typeof JournalEntry
  | typeof Macro
  | typeof RollTable
  | typeof Scene;

declare type AnyDocument = InstanceType<AnyDocumentType>;

declare class GenericCollection extends WorldCollection<AnyDocumentType, any> {}

interface LenientGlobalVariableTypes {
  game: never;
}

interface KeyboardEventContext {
  /** Custom field */
  _quick_insert_extra?: unknown;
}

interface KeybindingActionConfig {
  /** Custom field */
  textInput?: boolean;
}
