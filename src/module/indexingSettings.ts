import { IndexedDocumentTypes } from "./searchLib";
import { getSetting, setSetting } from "./settings";
import { ModuleSetting } from "./store/ModuleSettings";
import { i18n, permissionListEq } from "./utils";
import type { IndexingDisabledSetting } from "./store/IndexingDisabledType";

export class IndexingSettings extends FormApplication {
  static get defaultOptions(): FormApplicationOptions {
    return {
      ...super.defaultOptions,
      title: i18n("IndexingSettingsTitle"),
      id: "indexing-settings",
      template: "modules/quick-insert/templates/indexing-settings.hbs",
      resizable: true,
      width: 660,
    };
  }

  getData(): any {
    if (!game.packs) return null;

    const disabled = getSetting(ModuleSetting.INDEXING_DISABLED) as
      | IndexingDisabledSetting
      | undefined;
    return {
      documentTypes: IndexedDocumentTypes.map((type) => ({
        type,
        title: `DOCUMENT.${type}`,
        values: [1, 2, 3, 4].map((role) => ({
          role,
          disabled: disabled?.entities?.[type]?.includes(role),
        })),
      })),
      compendiums: [...game.packs.keys()].map((pack) => ({
        pack,
        values: [1, 2, 3, 4].map((role) => ({
          role,
          disabled: disabled?.packs?.[pack]?.includes(role),
        })),
      })),
    };
  }

  activateListeners(html: JQuery<HTMLElement>): void {
    super.activateListeners(html);
    // Set initial state for all
    const disabled = getSetting(ModuleSetting.INDEXING_DISABLED);
    Object.entries(disabled.packs).forEach(([pack, val]) => {
      const check = html.find(`[data-disable="${pack}"]`);
      if (permissionListEq(val as [], [1, 2, 3, 4])) {
        check.prop("checked", false);
      } else {
        check.prop("indeterminate", true);
      }
    });

    // Root check change -> updates regular checks
    (html.find("input.disable-pack") as JQuery<HTMLInputElement>).on(
      "change",
      function () {
        const compendium = this.dataset.disable;
        html
          .find(`input[name^="${compendium}."]`)
          .prop("checked", this.checked);
      }
    );

    // Regular check change -> updates root check
    (html.find(".form-fields input") as JQuery<HTMLInputElement>).on(
      "change",
      function () {
        const compendium = this.name.slice(0, -2);
        const checks = html
          .find(`input[name^="${compendium}."]`)
          .toArray() as HTMLInputElement[];
        if (checks.every((e: HTMLInputElement) => e.checked)) {
          html
            .find(`[data-disable="${compendium}"]`)
            .prop("checked", true)
            .prop("indeterminate", false);
        } else if (checks.every((e: HTMLInputElement) => !e.checked)) {
          html
            .find(`[data-disable="${compendium}"]`)
            .prop("checked", false)
            .prop("indeterminate", false);
        } else {
          html
            .find(`[data-disable="${compendium}"]`)
            .prop(
              "checked",
              checks.some((e: HTMLInputElement) => e.checked)
            )
            .prop("indeterminate", true);
        }
      }
    );

    // Deselect all button
    html.find("button.deselect-all").on("click", (e) => {
      e.preventDefault();
      e.stopPropagation();
      html
        .find(`.form-group.pack input[type="checkbox"]`)
        .prop("checked", false)
        .prop("indeterminate", false);
    });
    // Select all button
    html.find("button.select-all").on("click", (e) => {
      e.preventDefault();
      e.stopPropagation();
      html
        .find(`.form-group.pack input[type="checkbox"]`)
        .prop("checked", true)
        .prop("indeterminate", false);
    });
  }

  async _updateObject(
    event: Event,
    formData: Record<string, boolean>
  ): Promise<void> {
    const res: {
      entities: {
        [key: string]: number[];
      };
      packs: {
        [key: string]: number[];
      };
    } = {
      entities: {},
      packs: {},
    };

    for (const [name, checked] of Object.entries(formData)) {
      if (!checked) {
        const [base, middle, last] = name.split(".");
        if (last) {
          const pack = `${base}.${middle}`;
          res.packs[pack] = res.packs[pack] || [];
          res.packs[pack].push(parseInt(last));
        } else {
          const type = base;
          res.entities[type] = res.entities[type] || [];
          res.entities[type].push(parseInt(middle));
        }
      }
    }
    setSetting(ModuleSetting.INDEXING_DISABLED, res);
  }
}
